import discord
import os
import sys
import asyncio
import time
import json

from discord.ext import commands
from discord.utils import get

bot = commands.Bot(command_prefix='.', intents=discord.Intents.all())
TOKEN = 'OTY5NDg2NjQxODc2MDQxNzI4.GjW3FZ.TIfQA_qBbBMVXN6h4ENHvC4NfSX1dj2rJdvhP0'

BADWORDS = ['пидорас', 'негр', 'хуесос']
LINKS = ["https", "http", "://", "com", "ru", "ua", "net", "org", "shop"]

@bot.event
async def on_ready():
	print('- АККАУНТ БОТА -')
	print()
	print(f'Имя бота: {bot.user.name}')
	print(f'ID бота: {bot.user.id}')
	print(f'Токен бота: {TOKEN}')
	print()

	if not os.path.exists('users.json'):
		with open('users.json', 'w') as file:
			file.write('{}')
			file.close()

	for guild in bot.guilds:
		for member in guild.members:
			with open('users.json', 'r') as file:
				data = json.load(file)
				file.close

			with open('users.json', 'w') as file:
				data[str(member.id)] = {
					'WARNS': 0,
					'CAPS': 0
				}

				json.dump(data, file, indent=4)
				file.close()

@bot.event
async def on_message(message):
	WARN = BADWORDS + LINKS

	for i in range(0, len(WARN)):
		if WARN[i] in message.content.lower():
			await message.delete()
			with open('users.json', 'r') as file:
				data = json.load(file)
				file.close()

			with open('user.json', 'w') as file:		
				data[str(message.author.id)]['WARNS'] += 1
				json.dump(data, file, indent=4)

				file.close()

			emb = discord.Embed(
				title='Нарушение',
				description= f"*Ранне, у нарушителя было уже {data[str(message.author)]['WARNS'] - 1} нарушений, после 7 он будет заблокирован!*",
				timestamp=message.created_at
			)

			emb.add_field(name='Канал: ', value=message.channel.mention, inline=True)
			emb.add_field(name='Нарушитель:', value=message.author.mention, inline=True)
			emb.add_field(name='Тип нарушения:', value='Ругательства/ссылки', inline=True)

			await get(message.guild.text_channels, id=792834338852044831).send(embed=emb)

			if data[str(message.author.id)]['WARNS'] >= 7:
				await message.author.ban(reason='Вы привысили допустимое кол-во нарушений!')

	if message.content.isupper():
		with open('users.json', 'r') as file:
			data = json.load(file)
			file.close()

		with open('users.json', 'w') as file:
			data[str(message.author.id)]['CAPS'] += 1
			json.dump(data, file, indent=4)

		if data[str(message.author.id)]['CAPS'] >= 3:
			
			with open('users.json', 'w') as file:
				data[str(message.author.id)]['CAPS'] = 0
				data[str(message.author.id)]['WARNS'] += 1

				json.dump(data, file, indent=4)
				file.close()


			emb = discord.Embed(
				title='Нарушение',
				description=f"*Ранне, у нарушителя было уже {data[str(message.author.id)]['WARNS'] - 1} нарушений, после 7 он будет заблокирован!*",
				timestamp=message.created_at
			)

			emb.add_field(name='Канал: ', value=message.channel.mention, inline=True)
			emb.add_field(name='Нарушитель:', value=message.author.mention, inline=True)
			emb.add_field(name='Тип нарушения:', value='КАПС', inline=True)

			await get(message.guild.text_channels, id=792834338852044831).send(embed=emb)

			if data[str(message.author.id)]['WARNS'] >= 7:
				await message.author.ban(reason='Вы привысили допустимое кол-во нарушений!')

bot.run(TOKEN)